def read_datasets(filename):
    data = list()
    with open(filename) as file:  # opens path_source
        lines = file.readlines()
        lines.pop(0)
        for line in lines:
            line_content = line.split(';')
            ds = { 'name': line_content[0], 'birthday': line_content[2], 'email': line_content[14], 'salary': int(line_content[13].replace('.', '')) }
            data.append(ds)
    return data

def append_dataset(dataset):
    data = dataset.copy() # create a copy, so that we don't harm the original list
    # ...
    pass

