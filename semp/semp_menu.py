from semp.semp_data import read_datasets
data = list()

def print_choose():
    print("Please choose one of the following options:")


def print_not_developed():
    print("This function is not developed yet.")


def print_not_valid():
    print("This is not a valid option!")


def choose_between(start, end):
    print("Please enter a number between", start, "and", str(end) + "!")
    return input("Input: ")


def create_menu():
    while True:
        print_choose()
        print("1. import data from file\n"
              "2. add new single dataset (via console)\n"
              "0. back")
        selection = choose_between(0, 2)
        if selection == "1":
            filename = input("Welche Datei soll eingelesen werden?\n")
            global data; data = read_datasets(filename)
            break
        elif selection == "2":
            print_not_developed()
        elif selection == "0":
            break
        else:
            print_not_valid()


def print_datasets(data):
    print("ID", "Name", "E-Mail", "Birthday", "Salary")
    for num, ds in enumerate(data, start=1):
        print(num, ds['name'], ds['email'], ds['birthday'], ds['salary'])


def read_menu():
    while True:
        print_choose()
        print("1. show all datasets\n"
              "2. filter name\n"
              "3. show highest salary\n"
              "0. back")
        selection = choose_between(0, 3)
        if selection == "1":
            print_datasets(data)
            break
        elif selection == "2":
            print_not_developed()
        elif selection == "3":
            print_not_developed()
        elif selection == "0":
            break
        else:
            print_not_valid()


def update_menu():
    while True:
        print_choose()
        print("1. update single dataset\n"
              "0. back")
        selection = choose_between(0, 1)
        if selection == "1":
            print_not_developed()
        elif selection == "0":
            break
        else:
            print_not_valid()


def delete_menu():
    while True:
        print_choose()
        print("1. delete single row\n"
              "0. back")
        selection = choose_between(0, 1)
        if selection == "1":
            print_not_developed()
        elif selection == "0":
            break
        else:
            print_not_valid()


def main_menu():
    print("Welcome to the Employee Management Program (EMP)\n==================================================")
    while True:
        print_choose()
        print("1. create\n2. read\n3. update\n4. delete\n5. save/export\n0. end program")
        selection = choose_between(0, 5)
        if selection == "1":
            create_menu()
        elif selection == "2":
            read_menu()
        elif selection == "3":
            update_menu()
        elif selection == "4":
            delete_menu()
        elif selection == "5":
            print_not_developed()
        elif selection == "0":
            print("Good Bye!\n====================== END =======================")
            break
        else:
            print_not_valid()